﻿using AutoMapper;
using PromoCodeFactory.BusinessLogic.Interfaces;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Customer;
using PromoCodeFactory.Domain.Aggregates.PromoCodeManagement;
using PromoСodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;

namespace PromoCodeFactory.BusinessLogic.Mappings
{
    public class CustomerMapper : Profile, IMapperProfile
    {
        public CustomerMapper()
        {
            CreateMap<Customer, CustomerListDtoResponse>();

            CreateMap<Customer, CustomerDetailDtoResponse>();

            CreateMap<CustomerDtoRequest, Customer>();
        }
    }
}
