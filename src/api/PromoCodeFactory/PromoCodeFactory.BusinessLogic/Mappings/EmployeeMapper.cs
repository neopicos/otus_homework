using AutoMapper;
using PromoCodeFactory.BusinessLogic.Interfaces;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Employee;
using PromoCodeFactory.Domain.Aggregates.Administration;

namespace PromoCodeFactory.BusinessLogic.Mappings
{
    public class EmployeeMapper : Profile, IMapperProfile
    {
        public EmployeeMapper()
        {
            CreateMap<Employee, EmployeeListDtoResponse>();

            CreateMap<Employee, EmployeeDetailDtoResponse>();
        }
    }
}