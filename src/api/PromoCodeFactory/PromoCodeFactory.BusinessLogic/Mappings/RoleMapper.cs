using AutoMapper;
using PromoCodeFactory.BusinessLogic.Interfaces;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Role;
using PromoCodeFactory.Domain.Aggregates.Administration;

namespace PromoCodeFactory.BusinessLogic.Mappings
{
    public class RoleMapper : Profile, IMapperProfile
    {
        public RoleMapper()
        {
            CreateMap<Role, RoleDtoResponse>();
        }
    }
}