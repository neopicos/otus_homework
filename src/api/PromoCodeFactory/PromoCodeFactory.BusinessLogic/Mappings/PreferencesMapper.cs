using AutoMapper;
using PromoCodeFactory.BusinessLogic.Interfaces;
using PromocodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Preferences;
using PromoCodeFactory.Domain.Aggregates.PromoCodeManagement;

namespace PromoCodeFactory.BusinessLogic.Mappings
{
    public class PreferencesMapper : Profile, IMapperProfile
    {
        public PreferencesMapper()
        {
            CreateMap<Preference, PreferenceDtoResponse>();

            CreateMap<PreferenceDtoRequest, Preference>();
        }
    }
}