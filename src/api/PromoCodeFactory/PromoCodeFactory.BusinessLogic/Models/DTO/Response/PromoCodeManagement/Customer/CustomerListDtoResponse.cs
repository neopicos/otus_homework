﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Customer
{
    /// <summary>
    /// DTO: модель клиента для представления в виде списка
    /// </summary>
    public class CustomerListDtoResponse
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
    }
}
