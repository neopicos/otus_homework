using System;

namespace PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Preferences
{
    /// <summary>
    /// DTO: представление предпочтения
    /// </summary>
    public class PreferenceDtoResponse
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
    }
}