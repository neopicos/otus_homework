using System;
using PromoCodeFactory.Domain.Aggregates.Administration;

namespace PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Employee
{
    /// <summary>
    /// DTO: детальное представление сотрудника
    /// </summary>
    public class EmployeeDetailDtoResponse
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Идентификатор роли
        /// </summary
        public Guid RoleId { get; set; }

        /// <summary>
        /// Количество промокодов
        /// </summary>
        public int AppliedPromoCodesCount { get; set; }
    }
}