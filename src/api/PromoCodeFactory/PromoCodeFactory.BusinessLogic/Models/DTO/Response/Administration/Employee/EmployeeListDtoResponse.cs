using System;

namespace PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Employee
{
    /// <summary>
    /// DTO: список сотрудников
    /// </summary>
    public class EmployeeListDtoResponse
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Наименование роли
        /// </summary>
        public string RoleDescription { get; set; }

        /// <summary>
        /// Количество промокодов
        /// </summary>
        public int AppliedPromoCodesCount { get; set; }
    }
}