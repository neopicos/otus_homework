using System;

namespace PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Role
{
    /// <summary>
    /// DTO: представление роли
    /// </summary>
    public class RoleDtoResponse
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}