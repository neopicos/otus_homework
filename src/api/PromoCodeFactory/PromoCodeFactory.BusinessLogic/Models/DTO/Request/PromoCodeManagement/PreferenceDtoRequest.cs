﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PromocodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement
{
    /// <summary>
    /// DTO: модель запроса предпочтения
    /// </summary>
    public class PreferenceDtoRequest
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        [Required]
        [DisplayName("Наименование")]
        public string Name { get; set; }
    }
}
