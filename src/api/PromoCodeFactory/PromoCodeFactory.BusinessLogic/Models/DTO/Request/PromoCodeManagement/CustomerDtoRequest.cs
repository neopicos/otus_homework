using System;
using System.ComponentModel.DataAnnotations;

namespace PromoСodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement
{
    /// <summary>
    /// DTO: модель запроса клиента
    /// </summary>
    public class CustomerDtoRequest
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; } = Guid.Empty;

        /// <summary>
        /// Имя
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }
    }
}