using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Role;
using PromoCodeFactory.DataAccess.Repositories;
using PromoCodeFactory.Domain.Aggregates.Administration;

namespace PromoCodeFactory.BusinessLogic.Services.AdministrationServices
{
    public class RoleService : IRoleService
    {
        private readonly IRepository<Role> _roleRepository;
        private readonly IMapper _mapper;

        public RoleService(IRepository<Role> roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Получение списка всех ролей
        /// </summary>
        public async Task<List<RoleDtoResponse>> GetAllAsync()
        {
            var roles = await _roleRepository.GetAllAsync();

            var response = roles.Select(role => _mapper.Map<RoleDtoResponse>(role)).ToList();

            return response;
        }
    }
}