using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Employee;
using PromoCodeFactory.DataAccess.Repositories;
using PromoCodeFactory.Domain.Aggregates.Administration;

namespace PromoCodeFactory.BusinessLogic.Services.AdministrationServices
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _mapper;

        public EmployeeService(IRepository<Employee> employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение списка всех сотрудников
        /// </summary>
        public List<EmployeeListDtoResponse> GetAll()
        {
            var employees = 
                _employeeRepository.GetWithInclude(employee => employee.Role);

            var response = employees.Select(employee =>
                _mapper.Map<EmployeeListDtoResponse>(employee)).ToList();

            return response;
        }

        /// <summary>
        /// Получение сотрудника по идентификатору
        /// </summary>
        public async Task<EmployeeDetailDtoResponse> GetAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                throw new Exception("Employee not found");
            
            var response = _mapper.Map<EmployeeDetailDtoResponse>(employee);
            return response;
        }
    }
}