using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PromocodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Preferences;
using PromoCodeFactory.DataAccess.Repositories;
using PromoCodeFactory.Domain.Aggregates.PromoCodeManagement;

namespace PromoCodeFactory.BusinessLogic.Services.PromoCodeManagementServices
{
    public class PreferencesService : IPreferencesService
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public PreferencesService(IRepository<Preference> preferenceRepository, IMapper mapper)
        {
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Добавление нового предпочтения
        /// </summary>
        public async Task<PreferenceDtoResponse> AddAsync(PreferenceDtoRequest model)
        {
            model.Id = Guid.NewGuid();

            var preference = await _preferenceRepository.CreateAsync(
                _mapper.Map<Preference>(model));

            var response = _mapper.Map<PreferenceDtoResponse>(preference);

            return response;
        }

        /// <summary>
        /// Получение предпочтения по Id
        /// </summary>
        public async Task<PreferenceDtoResponse> GetByIdAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            if (preference == null)
                throw new Exception($"Preference with id [{id}] was not found");

            var response = _mapper.Map<PreferenceDtoResponse>(preference);

            return response;
        }

        /// <summary>
        /// Удаление предпочтения
        /// </summary>
        public async Task DeleteAsync(Guid id)
        {
            var preference = _preferenceRepository.GetByIdAsync(id);
            if (preference == null)
                throw new Exception($"Preference with id [{id}] was not found");

            await _preferenceRepository.DeleteAsync(id);
        }

        /// <summary>
        /// Получение списка предпочтений
        /// </summary>
        public async Task<List<PreferenceDtoResponse>> GetAllAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var response = preferences.Select(pref => 
                _mapper.Map<PreferenceDtoResponse>(pref)).ToList();

            return response;
        }

        /// <summary>
        /// Изменить данные существующего предпочтения
        /// </summary>
        public async Task<PreferenceDtoResponse> UpdateAsync(PreferenceDtoRequest model)
        {
            var editPreferene = _preferenceRepository.GetByIdAsync(model.Id);
            if (editPreferene == null)
                throw new Exception($"Preference with id [{model.Id}] was not found");

            await _preferenceRepository.UpdateAsync(_mapper.Map<Preference>(model));

            var response = _mapper.Map<PreferenceDtoResponse>(editPreferene);

            return response;
        }
    }
}