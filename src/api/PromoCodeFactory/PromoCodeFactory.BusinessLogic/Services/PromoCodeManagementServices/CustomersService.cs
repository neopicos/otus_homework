﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Customer;
using PromoCodeFactory.DataAccess.Repositories;
using PromoCodeFactory.Domain.Aggregates.PromoCodeManagement;
using PromoСodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;

namespace PromoCodeFactory.BusinessLogic.Services.PromoCodeManagementServices
{
    /// <summary>
    /// Сервис для работы с клиентами
    /// </summary>
    public class CustomersService : ICustomersService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IMapper _mapper;

        public CustomersService(IMapper mapper, 
            IRepository<Customer> customerRepository)
        {
            _mapper = mapper;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получение всех клиентов
        /// </summary>
        public async Task<List<CustomerListDtoResponse>> GetAllAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers
                .Select(customer => _mapper.Map<CustomerListDtoResponse>(customer)).ToList();

            return response;
        }

        /// <summary>
        /// Получение клиентов по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<CustomerDetailDtoResponse> GetById(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                throw new Exception($"Employee with {id} was not found.");

            var response = _mapper.Map<CustomerDetailDtoResponse>(customer);

            return response;
        }

        /// <summary>
        /// Удаление клиента по Id
        /// </summary>
        public async Task DeleteAsync(Guid id)
        {
            var isDeleted = await _customerRepository.DeleteAsync(id);
            if (!isDeleted)
                throw new Exception($"Employee with {id} was not found.");
        }

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        public async Task<CustomerDetailDtoResponse> CreateAsync(CustomerDtoRequest model)
        {
            var newCustomer = _mapper.Map<Customer>(model);
            newCustomer.Id = Guid.NewGuid();

            var response = _mapper
                .Map<CustomerDetailDtoResponse>(await _customerRepository.CreateAsync(newCustomer));

            return response;
        }

        /// <summary>
        /// Изменить данные существующего клиента
        /// </summary>
        public async Task<CustomerDetailDtoResponse> UpdateAsync(Guid id, CustomerDtoRequest model)
        {
            var editCustomer = await _customerRepository.GetByIdAsync(id);
            if (editCustomer == null)
                throw new Exception($"Employee with {id} was not found.");

            if (model.Id == Guid.Empty ||
                model.Id != id)
                throw new Exception($"Check customer Id.");

            var newEditCustomer = _mapper.Map<Customer>(model);
            await _customerRepository.UpdateAsync(newEditCustomer);
            
            var response = _mapper
                .Map<CustomerDetailDtoResponse>(newEditCustomer);

            return response;
        }
    }
}
