using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PromocodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Preferences;

namespace PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices
{
    public interface IPreferencesService
    {
        Task<List<PreferenceDtoResponse>> GetAllAsync();

        Task<PreferenceDtoResponse> GetByIdAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<PreferenceDtoResponse> AddAsync(PreferenceDtoRequest model);

        Task<PreferenceDtoResponse> UpdateAsync(PreferenceDtoRequest model);
    }
}