﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Customer;
using PromoСodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;

namespace PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices
{
    public interface ICustomersService
    {
        Task<List<CustomerListDtoResponse>> GetAllAsync();

        Task<CustomerDetailDtoResponse> GetById(Guid id);

        Task DeleteAsync(Guid id);

        Task<CustomerDetailDtoResponse> CreateAsync(CustomerDtoRequest model);

        Task<CustomerDetailDtoResponse> UpdateAsync(Guid id, CustomerDtoRequest model);
    }
}
