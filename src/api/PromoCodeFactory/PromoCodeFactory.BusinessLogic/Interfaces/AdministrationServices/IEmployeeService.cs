using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Employee;

namespace PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices
{
    public interface IEmployeeService
    {
        List<EmployeeListDtoResponse> GetAll();

        Task<EmployeeDetailDtoResponse> GetAsync([Required] Guid id);
    }
}