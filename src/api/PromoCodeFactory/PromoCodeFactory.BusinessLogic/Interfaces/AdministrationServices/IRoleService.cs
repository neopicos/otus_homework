using System.Collections.Generic;
using System.Threading.Tasks;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Role;

namespace PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices
{
    public interface IRoleService
    {
        Task<List<RoleDtoResponse>> GetAllAsync();
    }
}