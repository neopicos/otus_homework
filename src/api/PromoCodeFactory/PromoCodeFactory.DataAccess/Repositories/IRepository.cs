using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PromoCodeFactory.Domain.Aggregates;

namespace PromoCodeFactory.DataAccess.Repositories
{
    public interface IRepository<T>
        where T : BaseDomainEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includeProperties);

        Task<T> CreateAsync(T item);

        Task UpdateAsync(T item);

        Task<bool> DeleteAsync(Guid id);

        IEnumerable<T> GetWhere(Func<T, bool> predicate);

        IEnumerable<T> GetWithInclude(params Expression<Func<T, object>>[] includeProperties);

        IEnumerable<T> GetWithInclude(Func<T, bool> predicate,
            params Expression<Func<T, object>>[] includeProperties);
    }
}