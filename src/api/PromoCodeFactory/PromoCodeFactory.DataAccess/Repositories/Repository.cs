using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PromoCodeFactory.Domain.Aggregates;

namespace PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий
    /// </summary>
    public class Repository<T> : IRepository<T>
        where T : BaseDomainEntity
    {
        private readonly DataContext _dataContext;

        public Repository(DataContext dataContext)
        {
            this._dataContext = dataContext;
        }

        /// <summary>
        /// Получение всех записей
        /// </summary>
        /// <returns>Список объектов заданной сущности</returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().AsNoTracking().ToListAsync();
        }

        /// <summary>
        /// Получение всех записей с определенным условием
        /// </summary>
        /// <returns>Список объектов заданной сущности, соответствующих заданному условию</returns>
        public IEnumerable<T> GetWhere(Func<T, bool> predicate)
        {
            return  _dataContext.Set<T>().AsNoTracking().AsEnumerable().Where(predicate).ToList();
        }

        /// <summary>
        /// Получение всех записей заданной сущностью с включением вложености определенных полей
        /// </summary>
        public IEnumerable<T> GetWithInclude(params Expression<Func<T, object>>[] includeProperties)
        {
            return Include(includeProperties).ToList();
        }

        /// <summary>
        /// Получение всех записей заданной сущностью с включением вложености определенных полей
        /// </summary>
        public IEnumerable<T> GetWithInclude(Func<T, bool> predicate,
            params Expression<Func<T, object>>[] includeProperties)
        {
            var query = Include(includeProperties);

            return query.AsEnumerable().Where(predicate).ToList();
        }

        /// <summary>
        /// Получение записи по Id
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <param name="includeProperties">Необходимы для включения в сущность поля</param>
        /// <returns>Объект сущности</returns>
        public async Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includeProperties)
        {
            var query = Include(includeProperties);

            return await query.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }
        
        /// <summary>
        /// Создание новой записи
        /// </summary>
        /// <param name="item">Объект сущности</param>
        public async Task<T> CreateAsync(T item)
        {
            var result = await _dataContext.Set<T>().AddAsync(item);

            await _dataContext.SaveChangesAsync();
            
            return result.Entity;
        }

        /// <summary>
        /// Составление выражение запроса
        /// </summary>
        /// <param name="includeProperties"></param>
        /// <returns>Выражение запроса</returns>
        private IQueryable<T> Include(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dataContext.Set<T>();

            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        /// <summary>
        /// Изменение данных сузествующей записи
        /// </summary>
        /// <param name="item">Объект сущности</param>
        public async Task UpdateAsync(T item)
        {
            _dataContext.Entry(item).State = EntityState.Modified;
            await _dataContext.SaveChangesAsync();
        }

        /// <summary>
        /// Удаление записи по Id
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns>Признак успеха выполнения операции</returns>
        public async Task<bool> DeleteAsync(Guid id)
        {
            var item = await GetByIdAsync(id);
            if (item == null)
            {
                return false;
            }

            _dataContext.Set<T>().Remove(item);

            await _dataContext.SaveChangesAsync();
            
            return true;
        }
    }
}