﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;

namespace gRPCClient
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var httpHandler = new HttpClientHandler();

            httpHandler.ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;

            using var channel = GrpcChannel.ForAddress("https://localhost:5001",
                new GrpcChannelOptions()
                {
                    HttpHandler = httpHandler
                });

            var client = new CustomerGrpc.CustomerGrpcClient(channel);

            var replay = client.GetByIdAsync(new Id {Value = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0"});

            Console.WriteLine(replay.FirstName);
        }
    }
}
