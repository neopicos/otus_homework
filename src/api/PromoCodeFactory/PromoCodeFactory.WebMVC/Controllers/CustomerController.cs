﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;

namespace PromoCodeFactory.WebMVC.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomersService _customersService;

        public CustomerController(ICustomersService customersService)
        {
            _customersService = customersService;
        }
    }
}
