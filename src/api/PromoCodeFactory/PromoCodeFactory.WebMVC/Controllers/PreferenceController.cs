﻿using Microsoft.AspNetCore.Mvc;
using PromocodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace PromoCodeFactory.WebMVC.Controllers
{
    public class PreferenceController : Controller
    {
        private readonly IPreferencesService _preferencesService;

        public PreferenceController(IPreferencesService preferencesService)
        {
            _preferencesService = preferencesService;
        }

        [HttpGet]
        public async Task<IActionResult> IndexAsync()
        {
            return View("Index", await _preferencesService.GetAllAsync());
        }

        [HttpPost]
        public async Task<IActionResult> AddAsync(PreferenceDtoRequest model)
        {
            var response = await _preferencesService.AddAsync(model);

            return View("Index", await _preferencesService.GetAllAsync());
        }

        [HttpPost("{id:guid}")]
        public async Task<IActionResult> DeleteAsync([Required] Guid id)
        {
            await _preferencesService.DeleteAsync(id);

            return View("Index", await _preferencesService.GetAllAsync());
        }

        [HttpPost("{id:guid}")]
        public async Task<IActionResult> UpdateAsync([Required] Guid id, PreferenceDtoRequest model)
        {
            await _preferencesService.UpdateAsync(model);

            return View("Index", await _preferencesService.GetAllAsync());
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> UpdateAsync([Required] Guid id)
        {
            var preference = await _preferencesService.GetByIdAsync(id);

            return View("Update", preference);
        }
    }
}
