﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;

namespace PromoCodeFactory.WebMVC.Controllers
{
    public class PromoCodeController : Controller
    {
        private readonly IPromoCodesServices _promoCodesServices;

        public PromoCodeController(IPromoCodesServices promoCodesServices)
        {
            _promoCodesServices = promoCodesServices;
        }
    }
}
