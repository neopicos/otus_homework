using System;

namespace PromoCodeFactory.Domain.Aggregates
{
    public class BaseDomainEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }
    }
}