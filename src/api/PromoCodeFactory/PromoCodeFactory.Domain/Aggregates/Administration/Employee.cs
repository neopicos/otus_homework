using System;

namespace PromoCodeFactory.Domain.Aggregates.Administration
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class Employee : BaseDomainEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Роль
        /// </summary>
        public Role Role { get; set; }
        public Guid RoleId { get; set; }

        /// <summary>
        /// Количество промокодов
        /// </summary>
        public int AppliedPromoCodesCount { get; set; }
    }
}