namespace PromoCodeFactory.Domain.Aggregates.Administration
{
    /// <summary>
    /// Роль
    /// </summary>
    public class Role : BaseDomainEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}