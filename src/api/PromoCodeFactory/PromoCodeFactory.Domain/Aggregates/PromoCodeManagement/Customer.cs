using System.Collections.Generic;

namespace PromoCodeFactory.Domain.Aggregates.PromoCodeManagement
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Customer : BaseDomainEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Предпочтения
        /// </summary>
        public ICollection<CustomerPreference> Preferences { get; set; }
    }
}