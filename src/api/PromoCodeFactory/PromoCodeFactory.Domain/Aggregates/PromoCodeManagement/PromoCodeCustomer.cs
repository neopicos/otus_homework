using System;

namespace PromoCodeFactory.Domain.Aggregates.PromoCodeManagement
{
    /// <summary>
    /// Промокоды клиентов
    /// </summary>
    public class PromoCodeCustomer : BaseDomainEntity
    {
        /// <summary>
        /// Промокод
        /// </summary>
        public virtual PromoCode PromoCode { get; set; }
        public Guid PromoCodeId { get; set; }
        
        /// <summary>
        /// Клиент
        /// </summary>
        public Customer Customer { get; set; }
        public Guid CustomerId { get; set; }
    }
}