namespace PromoCodeFactory.Domain.Aggregates.PromoCodeManagement
{
    /// <summary>
    /// Предпочтение
    /// </summary>
    public class Preference : BaseDomainEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
    }
}