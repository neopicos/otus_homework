using System;

namespace PromoCodeFactory.Domain.Aggregates.PromoCodeManagement
{
    /// <summary>
    /// Лимит по промокодам у партнера
    /// </summary>
    public class PartnerPromoCodeLimit : BaseDomainEntity
    {
        /// <summary>
        /// Партен
        /// </summary>
        public Partner Partner { get; set; }
        public Guid PartnerId { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Дата закрытия
        /// </summary>
        public DateTime? CancelDate { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Лимит
        /// </summary>
        public int Limit { get; set; }

    }
}