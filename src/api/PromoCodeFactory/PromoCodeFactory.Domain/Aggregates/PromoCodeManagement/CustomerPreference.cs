using System;

namespace PromoCodeFactory.Domain.Aggregates.PromoCodeManagement
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    public class CustomerPreference : BaseDomainEntity
    {
        /// <summary>
        /// Клиент
        /// </summary>
        public Customer Customer { get; set; }
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Предпочтение
        /// </summary>
        public Preference Preference { get; set; }
        public Guid PreferenceId { get; set; }
    }
}