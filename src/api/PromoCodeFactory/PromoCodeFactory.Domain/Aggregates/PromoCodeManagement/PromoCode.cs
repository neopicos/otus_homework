using System;
using System.Collections;
using System.Collections.Generic;
using PromoCodeFactory.Domain.Aggregates.Administration;

namespace PromoCodeFactory.Domain.Aggregates.PromoCodeManagement
{
    /// <summary>
    /// Промокод
    /// </summary>
    public class PromoCode : BaseDomainEntity
    {
        /// <summary>
        /// Код
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Информация
        /// </summary>
        public string ServiceInfo { get; set; }

        /// <summary>
        /// Дата начала
        /// </summary>
        public DateTime BeginTime { get; set; }
        
        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Наименование менеджера
        /// </summary>
        public string PartnerName { get; set; }

        /// <summary>
        /// Менеджер
        /// </summary>
        public Employee PartnerManager { get; set; }
        public Guid PartnerManagerId { get; set; }

        /// <summary>
        /// Предпочтение
        /// </summary>
        public Preference Preference { get; set; }
        public Guid PreferenceId { get; set; }
    }
}