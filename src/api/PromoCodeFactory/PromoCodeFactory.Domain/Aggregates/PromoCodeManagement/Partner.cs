using System.Collections.Generic;

namespace PromoCodeFactory.Domain.Aggregates.PromoCodeManagement
{
    /// <summary>
    /// Партнер
    /// </summary>
    public class Partner : BaseDomainEntity
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Количество выданных промокодов
        /// </summary>
        public int NumberIssuedPromoCodes { get; set; }
        
        /// <summary>
        /// Признак активности
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Лимит по промокодам
        /// </summary>
        public ICollection<PartnerPromoCodeLimit> PartnerLimits { get; set; }
    }
}