﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Customer;

namespace PromoCodeFactory.Hubs
{
    public class CustomersHub : Hub
    {
        private readonly ICustomersService _customersService;

        public CustomersHub(ICustomersService customersService)
        {
            _customersService = customersService;
        }

        /// <summary>
        /// Получение всех клиентов
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetAllCustomersAsync()
        {
            var response = await _customersService.GetAllAsync();

            await this.Clients.All.SendAsync("GetAllCustomersAsync");

            var options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
                WriteIndented = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true
            };

            return JsonSerializer.Serialize(response, options);
        }
    }
}
