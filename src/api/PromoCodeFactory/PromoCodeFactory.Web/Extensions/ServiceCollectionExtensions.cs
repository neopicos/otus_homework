using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PromoCodeFactory.BusinessLogic.Interfaces;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Services.AdministrationServices;
using PromoCodeFactory.BusinessLogic.Services.PromoCodeManagementServices;
using PromoCodeFactory.DataAccess;
using PromoCodeFactory.DataAccess.Data;
using PromoCodeFactory.DataAccess.Repositories;

namespace PromoCodeFactory.Extensions
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Добавление сервисов слоя данных
        /// </summary>
        public static void AddDataAccessServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>((options) =>
            {
                var connect = configuration.GetConnectionString("Postgres"); 
                options.UseNpgsql(connect);
            });

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IDbInitializer, DbInitializer>();
        }

        /// <summary>
        /// Добавление сервисов слоя бизнес-логики
        /// </summary>
        public static void AddBusinessLogicServices(this IServiceCollection services)
        {
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IPreferencesService, PreferencesService>();
            services.AddScoped<ICustomersService, CustomersService>();
            services.AddScoped<IPromoCodesServices, PromoCodesService>();
            services.AddScoped<IPartnersService, PartnersService>();
        }

        /// <summary>
        /// Добавление сторонних сервисов
        /// </summary>
        public static void AddIntegratedServices(this IServiceCollection services)
        {
            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCodeFactory";
                options.Version = "1.0";
            });

            services.AddCors();

            services.AddGrpc(options => 
            {
                options.EnableDetailedErrors = true;
            });

            services.AddSignalR();

            services.AddAutoMapper(typeof(IMapperProfile));
        }
    }
}