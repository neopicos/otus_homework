using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PromoCodeFactory.DataAccess.Data;
using PromoCodeFactory.Extensions;
using PromoCodeFactory.gRPC.Services;
using PromoCodeFactory.Hubs;

namespace PromoCodeFactory
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDataAccessServices(this.Configuration);
            services.AddBusinessLogicServices();
            services.AddIntegratedServices();
            
            services.AddControllers();
        }
        
        public void Configure(IApplicationBuilder app,
            IWebHostEnvironment env,
            IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(settings =>
            {
                settings.DocExpansion = "list";
            });
            app.UseReDoc(x => x.Path = "/redoc");
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<CustomersHub>("/GetCustomersHub");
                endpoints.MapGrpcService<CustomersGrpcService>();
                endpoints.MapControllers();
            });
            
            dbInitializer.InitializeDb();
        }
    }
}