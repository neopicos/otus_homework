﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodesFactory.Web;
using PromoСodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;

namespace PromoCodeFactory.gRPC.Services
{
    /// <summary>
    /// gRPC-сервис (api) для работы с клиентами
    /// </summary>
    public class CustomersGrpcService 
        : CustomerGrpc.CustomerGrpcBase
    {
        private readonly ICustomersService _customersService;

        public CustomersGrpcService(ICustomersService customersService, IMapper mapper)
        {
            _customersService = customersService;
        }

        /// <summary>
        /// Получение всех клиентов
        /// </summary>
        public override async Task GetAllAsync(Empty request,
            IServerStreamWriter<CustomerGrpsListDtoResponse> responseStream,
            ServerCallContext context)
        {
            var customers = await _customersService.GetAllAsync();

            var gRpcResponseCollect = customers.Select(customerDto => new CustomerGrpsListDtoResponse()
            {
                Id = new Id {Value = customerDto.Id.ToString()},
                Email = customerDto.Email,
                FirstName = customerDto.FirstName,
                LastName = customerDto.LastName
            }).ToList();

            foreach (var customer in gRpcResponseCollect)
                await responseStream.WriteAsync(customer);
        }

        /// <summary>
        /// Получение клиента по Id
        /// </summary>
        public override async Task<CustomerGrpsDetailDtoResponse> GetByIdAsync(Id request, ServerCallContext context)
        {
            var customer = await _customersService.GetById(Guid.Parse(request.Value));

            var gRpcResponse = new CustomerGrpsDetailDtoResponse()
            {
                Id = new Id {Value = customer.Id.ToString()},
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            return gRpcResponse;
        }

        /// <summary>
        /// Удаление клиента по Id
        /// </summary>
        public override async Task<Empty> DeleteAsync(Id request, ServerCallContext context)
        {
            await _customersService.DeleteAsync(Guid.Parse(request.Value));
            return new Empty();
        }

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        public override async Task<CustomerGrpsDetailDtoResponse> CreateAsync(CreateOrEditCustomerGrpsDtoRequest request,
            ServerCallContext context)
        {
            var customerDtoRequest = new CustomerDtoRequest()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            var newCustomer = await _customersService.CreateAsync(customerDtoRequest);

            var gRpcResponse = new CustomerGrpsDetailDtoResponse()
            {
                Id = new Id {Value = newCustomer.Id.ToString()},
                FirstName = newCustomer.FirstName,
                LastName = newCustomer.LastName,
                Email = newCustomer.Email
            };

            return gRpcResponse;
        }

        /// <summary>
        /// Изменение данных существующего клиента
        /// </summary>
        public override async Task<Empty> EditAsync(CreateOrEditCustomerGrpsDtoRequest request, ServerCallContext context)
        {
            var customerDtoRequest = new CustomerDtoRequest()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };
            
            await _customersService.UpdateAsync(Guid.Parse(request.Id.Value), customerDtoRequest);

            return new Empty();
        }
    }
}
