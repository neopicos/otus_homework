using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Preferences;

namespace PromoCodeFactory.Controllers.PromoCodeManagement
{
    /// <summary>
    /// API предпочтений
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IPreferencesService _preferencesService;

        public PreferencesController(IPreferencesService preferencesService)
        {
            _preferencesService = preferencesService;
        }
        
        /// <summary>
        /// Получение списка предпочтений
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(List<PreferenceDtoResponse>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _preferencesService.GetAllAsync());
        }
    }
}