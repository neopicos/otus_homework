﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.PromoCodeManagement.Customer;
using PromoСodeFactory.BusinessLogic.Models.DTO.Request.PromoCodeManagement;

namespace PromoCodeFactory.Controllers.PromoCodeManagement
{
    /// <summary>
    /// API клиентов
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomersService _customersService;

        public CustomerController(ICustomersService customersService)
        {
            _customersService = customersService;
        }

        /// <summary>
        /// Получение всех клиентов
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _customersService.GetAllAsync());
        }

        /// <summary>
        /// Получение клиента по Id
        /// </summary>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(CustomerDetailDtoResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById([Required] Guid id)
        {
            return Ok(await _customersService.GetById(id));
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteAsync([Required] Guid id)
        {
            await _customersService.DeleteAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync(CustomerDtoRequest model)
        {
            return Created("", await _customersService.CreateAsync(model));
        }
        
        /// <summary>
        /// Изменить данные существующего трансформатора
        /// </summary>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateAsync([Required]Guid id, CustomerDtoRequest model)
        {
            return Ok(await _customersService.UpdateAsync(id, model));
        }
    }
}
