﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PromoCodeFactory.Controllers.PromoCodeManagement
{
    /// <summary>
    /// API промокодов
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class PromoCodesController : ControllerBase
    {
    }
}
