using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Role;

namespace PromoCodeFactory.Controllers.Administration
{
    /// <summary>
    /// API роли
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Получение списка ролей
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(List<RoleDtoResponse>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _roleService.GetAllAsync());
        }
    }
}