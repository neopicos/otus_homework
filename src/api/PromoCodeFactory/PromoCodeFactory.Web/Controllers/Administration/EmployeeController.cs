using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PromoCodeFactory.BusinessLogic.Interfaces.PromoCodeManagementServices;
using PromoCodeFactory.BusinessLogic.Models.DTO.Response.Administration.Employee;

namespace PromoCodeFactory.Controllers.Administration
{
    /// <summary>
    /// API сотрудника
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
    
        /// <summary>
        /// Получение списка всех сотрудников
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(List<EmployeeListDtoResponse>), StatusCodes.Status200OK)]
        public IActionResult GetAll()
        {
            return Ok(_employeeService.GetAll());
        }

        /// <summary>
        /// Получить сотрудника по Id
        /// </summary>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(EmployeeDetailDtoResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAsync([Required]Guid id)
        {
            return Ok(await _employeeService.GetAsync(id));
        }
    }
}