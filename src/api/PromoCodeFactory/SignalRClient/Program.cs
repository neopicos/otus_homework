﻿using System;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.AspNetCore.SignalR.Client;

namespace SignalRClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44363/GetCustomersHub")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();

            var result = await connection.InvokeAsync<string>("GetAllCustomersAsync");

            Console.WriteLine(result);
            Console.ReadLine();

            await connection.StopAsync();
        }
    }
}
