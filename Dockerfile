﻿FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

# Copy and build in the intermediate image
COPY ./src /app
RUN dotnet publish /app/api/PromoCodeFactory/PromoCodeFactory.Web -c Release -o ./build/release

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
ENV ASPNETCORE_URLS http://+:5000

COPY --from=build-env /app/build/release .
ENTRYPOINT ["dotnet", "PromoCodeFactory.Web.dll"]
